const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy,
  ExtractJwt = require('passport-jwt').ExtractJwt;
const opts = {};
const { Staff } = require('../models');
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_SECRET;
passport.use(
  new JwtStrategy(opts, async (jwt_payload, done) => {
    console.log(jwt_payload);
    Staff.findOne({
      where: {
        id: jwt_payload.id,
        name: jwt_payload.name,
        city: jwt_payload.city,
        role_id: jwt_payload.role_id,
        email: jwt_payload.email
      }
    })
      .then((staff) => done(null, staff))
      .catch((err) => done(err, false));
  })
);

module.exports = passport.authenticate('jwt', { session: false });
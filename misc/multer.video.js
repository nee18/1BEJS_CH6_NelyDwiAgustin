const multer = require('multer');
const fs = require('fs');

  const videoStorage  = multer.diskStorage({
    destination: (req, file, cb) => {
      const fileLocation = './public/static/videos';
      if (!fs.existsSync(fileLocation)) fs.mkdirSync(fileLocation, { recursive: true });
      cb(null, fileLocation);
    },
    filename: (req, file, cb) => {
      const fileType = file.mimetype.split('/')[1];
      cb(null, file.fieldname + '-' + Date.now() + `.${fileType}`);
  }
})
  
const videoUpload = multer({
    storage: videoStorage,
    limits: {
    fileSize: 10000000 // 10000000 Bytes = 10 MB
    },
    fileFilter(req, file, cb) {
      // upload only mp4 and mkv format
      if (!file.originalname.match(/\.(mp4|MPEG-4|mkv)$/)) { 
         return cb(new Error('Please upload a video'))
      }
      cb(undefined, true)
   }
})

module.exports = videoUpload;
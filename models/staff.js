'use strict';
const bcrypt = require('bcrypt');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Staff extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Staff.hasOne(models.Account, {
        foreignKey: 'staff_id'
      });
      Staff.belongsTo(models.Role, {
        foreignKey: 'role_id'
      });
    }
  }
  Staff.init({
    name: DataTypes.STRING,
    city: DataTypes.STRING,
    role_id: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    video: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Staff',
    hooks: {
      beforeCreate: (staff, options) => {
        staff.password = bcrypt.hashSync(staff.password, +process.env.SALT_ROUNDS);
        return staff;
      }
    }
  });
  return Staff;
};
const { Role } = require('../models');

const getAllRoles = async (req, res) => {
    try {
        // let { page, row } = req.query;

        // page -= 1;

        const options = {
            attributes: ['id', 'name'],
            // offset: page,
            // limit: row,
        };

        const allRoles = await Role.findAll(options);

        res.status(200).json({
            status: 'success',
            result: allRoles,
        });
    } catch (error) {
        return res.status(500).json({
          status: 'error',
          message: err.message
        }) 
    }
}

const getRoleById = async (req, res) => {
    const foundRole = await Role.findByPk(req.params.id);

    if (!foundRole) {
        return res.status(404).json({
          message: `Staff dengan id ${req.params.id} tidak ditemukan`
        })
    }
    res.status(200).json({
        status: 'success',
        result: foundRole
    })
}

const createRole = async (req, res) => {
    try {
        const { name } = req.body;
    
        const createdRole = await Role.create({
            name: name
        });
        res.status(201).json({
            status: 'success',
            result: createdRole
        });
    } catch (error) {
        return res.status(500).json({
          status: 'error',
          message: err.message
        })
    }
}

const updateRole = async (req, res) => {
  try{
      const { name } = req.body;
    
      const updatedRole = await Role.update({
          name:name
      }, {
        where: {
          id: req.params.id
        } 
      });
      if (!updatedRole[0]) {
        return res.status(404).json({
          message: `Role dengan id ${req.params.id} tidak ditemukan`
      })
      }
      res.status(200).json({ 
          status: 'success',
          result: updatedRole
      })
  } catch(err){
    return res.status(500).json({
      status: 'error',
      message: err.message
    })
  }
}

const deleteRole = async (req, res) => {
  try{
      const deletedRole = await Role.destroy({
          where: {
              id: req.params.id
          }
      });
      if (!deletedRole) {
        return res.status(404).json({
          message: `Role dengan id ${req.params.id} tidak ditemukan`
      })
      }
      res.status(200).json({ 
          status: 'success',
          message: 'Yosha'
      })
  } catch(err){
    return res.status(500).json({
      status: 'error',
      message: err.message
    })
  }
}

module.exports = {
  getAllRoles,
  getRoleById,
  createRole,
  updateRole,
  deleteRole
}
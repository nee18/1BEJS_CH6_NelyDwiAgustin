const { Staff } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const getAllStaffs = async (req, res) => {
    try {
        // let { page, row } = req.query;

        // page -= 1;

        const options = {
            attributes: ['id', 'name', 'city', 'role_id', 'email', 'password'],
            // offset: page,
            // limit: row,
        };

        const allStaffs = await Staff.findAll(options);

        return res.status(200).json({
            status: 'success',
            result: allStaffs,
        });
    } catch (error) {
        return res.status(500).json({
          status: 'error',
          message: err.message
        }) 
    }
}

const getStaffById = async (req, res) => {
    const foundStaff = await Staff.findByPk(req.params.id);

    if (!foundStaff) {
        return res.status(404).json({
          message: `Staff dengan id ${req.params.id} tidak ditemukan`
        })
    }
    res.status(200).json({
        status: 'success',
        result: foundStaff
    })
}

const register = async (req, res) => {
  const { name, city, role_id, email, password, video_url } = req.body;
  const createdStaff = await Staff.create({
    name: name,
    city: city,
    role_id: role_id,
    email: email,
    password: password,
    video: video_url
  });
  return res.status(201).json({
    status: 'Staff created successfully',
    data: createdStaff.id
  });
};

const login = async (req, res) => {
  const { email, password } = req.body;
  const foundStaff = await Staff.findOne({
    where: {
      email: email
    }
  });
  const isValidPassword = bcrypt.compareSync(password, foundStaff.password);
  if (isValidPassword) {
    const payload = {
      id: foundStaff.id,
      name: foundStaff.name,
      city: foundStaff.city,
      role_id: foundStaff.role_id,
      email: foundStaff.email
    };
    const token = jwt.sign(payload, process.env.JWT_SECRET, {
      expiresIn: '1h'
    });
    return res.status(200).json({
      token: token
    });
  };
  return res.status(400).json({
    status: 'Failed',
    message: 'Wrong email or password'
  });
};


const createStaff = async (req, res) => {
    try {
        const { name, city, role_id, email, password } = req.body;
    
        const createdStaff = await Staff.create({
            name: name,
            city: city,
            role_id: role_id,
            email: email,
            password: password
        });
        res.status(201).json({
            status: 'success',
            result: createdStaff
        });
    } catch (error) {
        return res.status(500).json({
          status: 'error',
          message: err.message
        })
    }
}

const updateStaff = async (req, res) => {
  try{
      const { name, city, role_id, email, password } = req.body;
    
      const updatedStaff = await Staff.update({
          name: name,
          city: city,
          role_id: role_id,
          email: email,
          password: password
      }, {
        where: {
          id: req.params.id
        } 
      });
      if (!updatedStaff[0]) {
        return res.status(404).json({
          message: `Staff dengan id ${req.params.id} tidak ditemukan`
      })
      }
      res.status(200).json({ 
          status: 'success',
          result: updatedStaff
      })
  } catch(err){
    return res.status(500).json({
      status: 'error',
      message: err.message
    })
  }
}

const deleteStaff = async (req, res) => {
  try{
      const deletedStaff = await Staff.destroy({
          where: {
              id: req.params.id
          }
      });
      if (!deletedStaff) {
        return res.status(404).json({
          message: `Staff dengan id ${req.params.id} tidak ditemukan`
      })
      }
      res.status(200).json({ 
          status: 'success',
          message: 'Yosha'
      })
  } catch(err){
    return res.status(500).json({
      status: 'error',
      message: err.message
    })
  }
}

module.exports = {
  getAllStaffs,
  getStaffById,
  register,
  login,
  createStaff,
  updateStaff,
  deleteStaff
}
require('dotenv').config();
const express     = require('express');
const routes      = require('./routes');
const app         = express();
const morgan      = require("morgan");
const port        = process.env.PORT || 4000;
const bodyParser  = require("body-parser");
const swaggerJSON = require('./api-documentation/swagger-with-autogen.json')
const swaggerUi   = require('swagger-ui-express')

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerJSON))

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// app.use(`${process.env.BASE_URL}`, routes)
app.use('/tesuto', routes)
app.use((err, req, res, next) => {
    console.log(err);
    if (err === 'App tidak lengkap') {
        res.status(400).json({
            status: 'Bad Request',
            message: err
        })
    } else if (err === `Gagal membuat App baru`) {
        res.status(400).json({
            status: 'Not Found',
            message: err.message
        });
    } 
    else {
        res.status(500).json({
            status: err.name,
            message: err.message
        });
    }
  
  })
// app.listen(port, () => {
//     console.log(`app is listening on port ${port}`);
// })

module.exports = app;
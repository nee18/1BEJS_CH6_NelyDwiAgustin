const { email, password } = require('../admin/admin.json')
const authenticate = (req, res, next) => {
    if (req.email !== email){
        return res.status(403).json({
            message: 'Email is incorrect'
        })
    }
    if (req.password !== password){
        return res.status(403).json({
            message: 'Password is incorrect'
        })
    }
    next()
}
module.export = {
    authenticate
}
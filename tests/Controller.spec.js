const { sequelize } = require('../models');

const getMasterData = (path) => {
    return require(path).map((eachData) => {
        eachData.createdAt = new Date();
        eachData.updatedAt = new Date();
        return eachData;
    });
}

const staffsData =  getMasterData('../masterdata/staffs.json');
const accountsData = getMasterData('../masterdata/accounts.json');
const rolesData = getMasterData('../masterdata/roles.json');

beforeAll(async () => {
    try {
        await sequelize.queryInterface
            .bulkInsert('Staffs', staffsData, {});
        await sequelize.queryInterface
            .bulkInsert('Accounts', accountsData, {});
        await sequelize.queryInterface
            .bulkInsert('Roles', rolesData, {});
    } catch (error) {
        console.log(error);
    }
})
// afterAll(async () => {
//     try {
//         await sequelize.queryInterface
//             .bulkDelete('Roles', null, { truncate: true, restartIdentity: true });
//         await sequelize.queryInterface
//             .bulkDelete('Accounts', null, { truncate: true, restartIdentity: true });
//         await sequelize.queryInterface
//             .bulkDelete('Staffs', null, { truncate: true, restartIdentity: true });
//         await sequelize.close();
//     } catch (error) {
//         console.log(error);
//     }
// })

const staffTest = require('./staffControllerTest');
const accountTest = require('./accountControllerTest');
const roleTest  = require('./roleControllerTest');

describe('run test sequentially', () => {
    staffTest();
    accountTest();
    roleTest();
})


const accountTest = () => {
        const app = require('../app');
        const request = require('supertest');

        //positive tests
        describe('getAllAccounts function', () => { // get all account
            it('get all accounts data with success status, show data', async () => {
                const response = await request(app).get(`/tesuto/account`);
                const { status, result } = response.body;
                expect(response.status).toBe(200);
                expect(status).toBe('success');
                expect(result.length).toBeGreaterThan(0);
            });
        });
        
        describe('getAccountById function', () => { // get account by id
            it('with success status, show data', async () => {
                const response = await request(app).get(`/tesuto/account/2`);
                const { status, result } = response.body;
                expect(response.status).toBe(200);
                expect(status).toBe('success');
                expect(result).toBeDefined();
            });
        });
        
        // negative testss
        // get account data by Id
        describe('getAccountById function', () => { 
            it('get account data by Id with status Bad Request, show error details', async () => {
                const response = await request(app).get(`/tesuto/account/200`);
                const { message } = response.body;
                expect(response.status).toBe(404);
                expect(message).toBe(response.body.message);
            })
        });
        
        // create account 
        describe('createAccount function', () => { 
            it('create account data with status 404, show error details', async () => {
                const body = {
                    "balance": 2,
                    "account_id": 100
                }
                const response = await request(app).post(`/tesuto/account`)
                    .send(body);
                const { message } = response.body;
                expect(message).toBe(response.body.message);
            });
        });

        // update account
        describe('updateAccount function', () => { 
            it('update account data with status 404, show error details', async () => {
                const body = {
                    "balance": 2,
                    "account_id": 100
                }
                const response = await request(app).put(`/tesuto/account/100`)
                .send(body);
                const { message } = response.body;
                expect(message).toBe(response.body.message);
            });
        });
        
        // delete account with invalid id
        describe('deleteAccount function', () => { 
            it('delete account data by Id with status 404, show error message', async () => {
                const response = await request(app).delete(`/tesuto/account/100`);
                const { message } = response.body;
                expect(response.status).toBe(404);
                expect(message).toBe(response.body.message);
            });
        });
    }

    module.exports = accountTest;
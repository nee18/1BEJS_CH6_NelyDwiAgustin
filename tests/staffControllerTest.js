const { Result } = require('express-validator');

const staffTest = () => {
        const app = require('../app');
        const request = require('supertest');
    
        //positive tests
        describe('getAllStaffs function', () => { // get all staff
            it('get all staffs data with success status, show data', async () => {
                const response = await request(app).get(`/tesuto/staff`);
                const { status, result } = response.body;
                expect(response.status).toBe(401);
                // expect(status).toBe('success');
                // expect(result.length).toBeGreaterThan(0);
            });
        });
        
        describe('getStaffById function', () => { // get staff by id
            it('get staff data by Id with success status, show data', async () => {
                const response = await request(app).get(`/tesuto/staff/2`);
                const { status, result } = response.body;
                expect(response.status).toBe(200);
                expect(status).toBe('success');
                expect(result).toBeDefined();
            });
        });
        
        // negative testss
        // get staff data by Id with user_id that doesn't exist
        describe('getStaffById function', () => { 
            it('get staff data by Id with success status, show data', async () => {
                const response = await request(app).get(`/tesuto/staff/200`);
                const { message } = response.body;
                expect(response.status).toBe(404);
                expect(message).toBe(response.body.message);
            })
        });

        // create staff with invalid
        describe('createStaff function', () => { 
            it('create staff data with status 404, show error details', async () => {
                const body = {
                    "name": "Nely Dwi A",
                    "city": "Magetan",
                    "role_id": 100
                }
                const response = await request(app).post(`/tesuto/staff`)
                    .send(body);
                const { message } = response.body;
                expect(message).toBe(response.body.message);
            });
        });

        // update staff with invalid id 
        describe('updateStaff function', () => { 
            it('update staff data with status 404, show error details', async () => {
                const body = {
                    "name": "Nely Dwi A",
                    "city": "Magetan",
                    "role_id": 100
                }
                const response = await request(app).put(`/tesuto/staff/100`)
                    .send(body);
                const { message } = response.body;
                expect(message).toBe(response.body.message);
            });
        });
        
        describe('deleteStaff function', () => { // delete staff with invalid id
            it('delete staff data by Id with status 404, show error message', async () => {
                const response = await request(app).delete(`/tesuto/staff/100`);
                const { message } = response.body;
                expect(response.status).toBe(404);
                expect(message).toBe(response.body.message);
            });
        });
    }
    
    module.exports = staffTest;
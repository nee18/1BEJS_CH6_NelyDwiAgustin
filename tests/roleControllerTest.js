const roleTest = () => {
        const app = require('../app');
        const request = require('supertest');

        //positive tests
        // get all role
        describe('getAllRoles function', () => {
            it('get all roles data with success status, show data', async () => {
                const response = await request(app).get(`/tesuto/role`);
                const { status, result } = response.body;
                expect(response.status).toBe(200);
                expect(status).toBe('success');
                expect(result.length).toBeGreaterThan(0);
            });
        });
        
        // get role by id
        describe('getRoleById function', () => { 
            it('get role data by Id with success status, show data', async () => {
                const response = await request(app).get(`/tesuto/role/2`);
                const { status, result } = response.body;
                expect(response.status).toBe(200);
                expect(status).toBe('success');
                expect(result).toBeDefined();
            });
        });

        // create role
        describe('createRole function', () => { 
            it('create role data with status 404, show error details', async () => {
                const body = {
                    "name":"Industrial Relation"
                }
                const response = await request(app).post(`/tesuto/role`)
                    .send(body);
                const { status, result } = response.body;
                expect(response.status).toBe(201);
                expect(status).toBe('success');
                expect(result).toBeDefined();
            });
        });
        
        // negative testss
        // get role data by Id 
        describe('getRoleById function', () => { 
            it('get role data by Id with status Bad Request, show error details', async () => {
                const response = await request(app).get(`/tesuto/role/200`);
                const { message } = response.body;
                expect(response.status).toBe(404);
                expect(message).toBe(response.body.message);
            })
        });
        
        // update role 
        describe('updateRole function', () => { 
            it('update role data with status 404, show error details', async () => {
                const body = {
                    "name":"Industrial Relation"
                }
                const response = await request(app).put(`/tesuto/role/100`)
                        .send(body);
                const { message } = response.body;
                expect(message).toBe(response.body.message);
            });
        });
        
        // delete role with invalid id
        describe('deleteRole function', () => { 
            it('delete role data by Id with status 404, show error message', async () => {
                const response = await request(app).delete(`/tesuto/role/100`);
                const { message } = response.body;
                expect(response.status).toBe(404);
                expect(message).toBe(response.body.message);
            });
        });
    }

    module.exports = roleTest;
const { body } = require('express-validator');
const { Staff } = require('../../models');

module.exports = {
    create: () => [
        body('balance')
            .isInt()
            .withMessage('Must be an int'),
        body('staff_id')
            .isInt()
            .custom((value) => {
            return Staff.findOne({
                where : {
                    id : value
                }
            }).then((user) => {
                if(!user) throw new Error(`Staff with id ${value} is not found`)
            })
        })
    ]
}
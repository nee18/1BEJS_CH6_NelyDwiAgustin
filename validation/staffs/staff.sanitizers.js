const { body } = require('express-validator');
const { Role } = require('../../models');

module.exports = {
    create: () => [
        body('name').toUpperCase(),
        body('city').toLowerCase(),
        body('role_id').toInt()
    ]
}
'use strict';
const accountsData = require('../masterdata/accounts.json');

module.exports = {
  async up (queryInterface, Sequelize) {
    const dataAccountsToBeSeeded = accountsData.map((eachAccountData) => {
      return {
        balance: eachAccountData.balance,
        staff_id: eachAccountData.staff_id,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    })
    await queryInterface.bulkInsert('Accounts', dataAccountsToBeSeeded, {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Accounts', null, { truncate: true, restartIdentity: true });
  }
};
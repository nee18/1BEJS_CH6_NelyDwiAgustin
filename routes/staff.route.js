const express              = require('express');
const router               = express.Router();
const staffRoutes          = require('../controllers/staff.controller');
const staffValidation      = require('../validation/staffs/staff.validation');
const validate             = require('../validation/validate');
const staffSanitizer       = require('../validation/staffs/staff.sanitizers');
const staffChecking        = require('../misc/passport');
const videoUpload          = require('../misc/multer.video');
const uploadWithCloudinary = require('../misc/cloudinary')

router.get('/', staffChecking,staffRoutes.getAllStaffs);
router.get('/:id', staffRoutes.getStaffById);
router.post('/register', videoUpload.single('video'), uploadWithCloudinary, staffRoutes.register);
router.post('/login', staffRoutes.login);
router.post('/', staffValidation.create(), staffSanitizer.create(), validate, staffRoutes.createStaff);
router.put('/:id', staffValidation.create(), staffSanitizer.create(), validate, staffRoutes.updateStaff);
router.delete('/:id', staffRoutes.deleteStaff);

module.exports = router;